import { Component } from '@angular/core';
import { AuthService } from './auth/services/auth/auth.service';
import { HomeComponent } from './home/components/home/home.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers:[AuthService]
})
export class AppComponent {
  title:String = 'Bienvenido a Ecalcula!';

  constructor (public authService:AuthService){
    authService.handleAuthentication();
  }

  ngOnInit(){ }

}
