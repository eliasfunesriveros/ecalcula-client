import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../../auth/services/auth/auth.service';

@Component({
  selector: 'app-main-bar',
  templateUrl: './main-bar.component.html',
  styleUrls: ['./main-bar.component.css'],
  providers: [ AuthService ]
})
export class MainBarComponent implements OnInit {

  constructor(public authService:AuthService) { }

  ngOnInit() {
  }

}
