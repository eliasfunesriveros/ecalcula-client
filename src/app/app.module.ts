import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { LoginComponent } from './auth/components/login/login.component';

import { RouterModule, Routes } from '@angular/router';
import { CallbackComponent } from './auth/components/callback/callback.component';
import { HomeComponent } from './home/components/home/home.component';
import { ProfileComponent } from './auth/components/profile/profile.component';
import { MainBarComponent } from './global_components/main_bar/component/main-bar/main-bar.component';
import { FooterBarComponent } from './global_components/footer_bar/component/footer-bar/footer-bar.component';
import { ContainerBarComponent } from './global_components/container_bar/component/container-bar/container-bar.component';
import { ProfilePersonaFisicaComponent } from './auth/components/profile-persona-fisica/profile-persona-fisica.component';
import { ProfilePersonaJuridicaComponent } from './auth/components/profile-persona-juridica/profile-persona-juridica.component';

const appRoutes:Routes = [
  // {
  //   path:'', 
  //   redirectTo:'home',
  //   pathMatch:'full'
  // },
  {
    path:'',
    component:HomeComponent
  },
  {
    path:'callback',
    component:CallbackComponent
  },
  {
    path:'home',
    component:HomeComponent
  },
  {
    path:'profile',
    component:ProfileComponent
  }
]


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    CallbackComponent,
    HomeComponent,
    ProfileComponent,
    MainBarComponent,
    FooterBarComponent,
    ContainerBarComponent,
    ProfilePersonaFisicaComponent,
    ProfilePersonaJuridicaComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
