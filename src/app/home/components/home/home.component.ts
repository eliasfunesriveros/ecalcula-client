import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../auth/services/auth/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(public authService:AuthService) {
    if(this.authService.isAuthenticated()){
      if(!this.authService.existUserProfile()){
        this.authService.getProfile(() => {});
      }
    }else{
      this.authService.logout();
      this.authService.router.navigate(['/home']);
    }
  }

  ngOnInit() {}

}
