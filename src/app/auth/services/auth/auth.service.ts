import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import 'rxjs/add/operator/filter';
import auth0 from 'auth0-js';
import { UserData } from '../../classes/user-data';

import { Http, Response, Headers, RequestOptions } from '@angular/http';



@Injectable()
export class AuthService {

  auth0 = new auth0.WebAuth({
    clientID: 'quy0oaT2lDRfLGibFkf37O1nT1JtUBTM',
    domain: 'door.auth0.com',
    responseType: 'token id_token',
    audience: 'https://door.auth0.com/userinfo',
    redirectUri: window.document.location.origin+'/callback',      
    scope: 'openid profile'
  })

  constructor(public router:Router, private http:Http) {}

  userProfile:any;

  public login(){
    this.auth0.authorize();
  }

  public handleAuthentication():void{
    this.auth0.parseHash((err, authResult) => {
      if(authResult && authResult.accessToken && authResult.idToken){
        window.location.hash = '';
        this.setSession(authResult);
        this.router.navigate(['/home']);
      }else if(err){
        this.router.navigate(['/home']);
      }    
    });
  }

  private setSession(authResult){
    const self = this;
    // Set the time that the access token will expire at
    const expiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
    localStorage.setItem('access_token', authResult.accessToken);
    localStorage.setItem('id_token', authResult.idToken);
    localStorage.setItem('expires_at', expiresAt);

    this.getProfile((err, profile) => {
      this.userProfile = profile
    });
  }

  public logout():void{
    // Remove tokens and expiry time from localStorage
    localStorage.removeItem('access_token');
    localStorage.removeItem('id_token');
    localStorage.removeItem('expires_at');
    this.userProfile = "";

    // auth0.isAuthenticated = false;
    // auth0.profile = null;
    // auth0.profilePromise = null;
    // auth0.idToken = null;
    // auth0.state = null;
    // auth0.accessToken = null;
    // auth0.tokenPayload = null;

    // Go back to the home route
    this.router.navigate(['/'])
  }

  public isAuthenticated():boolean{
    // Check whether the current time is past the
    // access token's expiry time
    const expiresAt = JSON.parse(localStorage.getItem('expires_at'));
    return new Date().getTime() < expiresAt;
  }

  public getProfile(cb): void {
    const self = this;
    const accessToken = localStorage.getItem('access_token');
    if(!accessToken){
      //  throw new Error('Access token must exist to fetch profile');
       this.router.navigate(['/home'])
    }else{
      if(!this.existUserProfile()){
        this.auth0.client.userInfo(accessToken, (err, profile) => {
          if(profile){
            self.userProfile = profile;
            self.setProfile(self.userProfile);
          }
          cb(err, profile)
        });
      }
    }
  }

  public setProfile(userProfile:any):void {
    if(userProfile != ""){
      var auth0id = userProfile.sub.substring(6);
      // var link = 'http://localhost/balanceApi/usuario/usuario';
      var link = 'http://localhost:9000/testPost';
      
      var data = new UserData();
       data.auth0id = auth0id.trim();
       data.email = userProfile.name;
       data.nickname = userProfile.nickname;

      this.http.post(link, data)
        .subscribe(res => {
           let us = res.json()
            userProfile.auth0id = us.auth0_id.trim();
            userProfile.id = us.id;
            userProfile.tipo_usuario = us.tipo_usuario;

        }, error => {
          console.log('error')
        })
      
    }
  }

  public existUserProfile():Boolean{
    if(typeof(this.userProfile) === "undefined"){
      return false
    }else{
      return true
    }
  }

}
