import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  constructor(public authService:AuthService) { 
    if(this.authService.isAuthenticated()){
      if(!this.authService.existUserProfile()){
        this.authService.getProfile(() => {});
      }
    }else{
      this.authService.logout();
      this.authService.router.navigate(['/home']);
    }
  }

  ngOnInit() {}

}
