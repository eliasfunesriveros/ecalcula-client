import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilePersonaJuridicaComponent } from './profile-persona-juridica.component';

describe('ProfilePersonaJuridicaComponent', () => {
  let component: ProfilePersonaJuridicaComponent;
  let fixture: ComponentFixture<ProfilePersonaJuridicaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfilePersonaJuridicaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilePersonaJuridicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
