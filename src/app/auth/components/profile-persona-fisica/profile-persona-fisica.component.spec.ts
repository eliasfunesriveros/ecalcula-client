import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilePersonaFisicaComponent } from './profile-persona-fisica.component';

describe('ProfilePersonaFisicaComponent', () => {
  let component: ProfilePersonaFisicaComponent;
  let fixture: ComponentFixture<ProfilePersonaFisicaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfilePersonaFisicaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilePersonaFisicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
