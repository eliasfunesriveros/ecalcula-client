import { EcalculaClientPage } from './app.po';

describe('ecalcula-client App', () => {
  let page: EcalculaClientPage;

  beforeEach(() => {
    page = new EcalculaClientPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
